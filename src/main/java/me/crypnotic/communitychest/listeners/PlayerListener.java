package me.crypnotic.communitychest.listeners;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import me.crypnotic.communitychest.CommunityChestPlugin;
import me.crypnotic.communitychest.managers.ChestsManager;

public class PlayerListener implements Listener {

	private static final int MAX_STACKS = 3;
	
	private final CommunityChestPlugin plugin;
	private final ChestsManager chestsManager;

	public PlayerListener(CommunityChestPlugin plugin) {
		this.plugin = plugin;
		this.chestsManager = plugin.getChestsManager();

	}

	@EventHandler
	public void onInventoryMove(InventoryMoveItemEvent event) {
		Inventory inventory = event.getDestination();
		if (!chestsManager.isCommunityChest(inventory)) {
			return;
		}
		ItemStack itemstack = event.getItem();
		HashMap<Integer, ? extends ItemStack> dataset = inventory.all(itemstack.getType());
		int current = dataset.values().stream().mapToInt(stack -> stack.getAmount()).sum();
		if (current >= (MAX_STACKS * itemstack.getMaxStackSize())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Inventory inventory = event.getInventory();

		if (inventory == null || !chestsManager.isCommunityChest(inventory) || event.getClickedInventory() == null) {
			return;
		}

		inventory = event.getClickedInventory();

		ItemStack item = event.getCurrentItem();
		if ((event.getAction() == InventoryAction.PLACE_ALL || event.getAction() == InventoryAction.PLACE_ONE
				|| event.getAction() == InventoryAction.PLACE_SOME)
				&& event.getClickedInventory().getType().equals(InventoryType.CHEST)) {
			ItemStack testItem = event.getCursor();
			if (testItem != null && testItem.getType() != null && !testItem.getType().equals(Material.AIR)) {
				item = testItem;
			}
		}

		if (item != null && item.getType() != null && item.getType() != Material.AIR) {

			if (event.getClickedInventory().getType().equals(InventoryType.PLAYER)) {
				inventory = event.getView().getTopInventory();
			}

			HashMap<Integer, ? extends ItemStack> dataset = inventory.all(item.getType());

			int maximum = MAX_STACKS * item.getMaxStackSize();
			int current = dataset.values().stream().mapToInt(stack -> stack.getAmount()).sum();
			int delta = maximum - current;
			int accepted = delta > item.getAmount() ? item.getAmount() : delta;
			int amount = item.getAmount();

			if (event.getClickedInventory().getType().equals(InventoryType.PLAYER)) {
				if (event.isShiftClick()) {
					if (amount > accepted) {
						event.setCancelled(true);
						ItemStack toAdd = item.clone();
						toAdd.setAmount(accepted);
						inventory.addItem(toAdd);
						item.setAmount(amount - accepted);
					}
				}
			} else if (event.getAction() == InventoryAction.PLACE_ALL || event.getAction() == InventoryAction.PLACE_ONE
					|| event.getAction() == InventoryAction.PLACE_SOME) {
				if (amount > accepted) {
					event.setCancelled(true);
					ItemStack toAdd = item.clone();
					toAdd.setAmount(accepted);
					inventory.addItem(toAdd);
					item.setAmount(amount - accepted);
				}
			}
		}
	}

	@EventHandler
	public void onInventoryDrag(InventoryDragEvent event) {
		Inventory inventory = event.getInventory();
		InventoryView inventoryView = event.getView();
		Inventory topInventory = inventoryView.getTopInventory();

		if (inventory == null || !chestsManager.isCommunityChest(inventory)) {
			return;
		}
		
		ItemStack typeStack = event.getNewItems().get(event.getNewItems().keySet().toArray()[0]);
		Integer leftOver = 0;
		if (event.getCursor() != null && event.getCursor().getAmount() != 0 && event.getCursor().getType() != Material.AIR) {
			leftOver = event.getCursor().getAmount();
		}
		
		HashMap<Integer, ItemStack> newItems = getNewItems(inventoryView, event.getNewItems());
		HashMap<Integer, ItemStack> oldItems = getOldItems(inventoryView, Arrays.asList(event.getNewItems().keySet().toArray()));
		Map<Integer, ItemStack> finalItems = event.getNewItems();
		
		Integer lastCommunitySlot = topInventory.getSize() - 1;
		
		event.setCursor(new ItemStack(Material.AIR));

		HashMap<Integer, ItemStack> setChest = new HashMap<>();
		HashMap<Integer, ItemStack> setPlayer = new HashMap<>();
		HashMap<Integer, Integer> rejectSlots = new HashMap<>();
		
		
		for(Integer slot : newItems.keySet()){
			ItemStack preItem = newItems.get(slot);
			int maximum = MAX_STACKS * preItem.getMaxStackSize();
			int chestTotal = 0;
			if(!setChest.isEmpty()){
				chestTotal = setChest.values().stream().mapToInt(stack -> stack.getAmount()).sum();
			}
			int current = chestTotal + topInventory.all(preItem.getType()).values().stream().mapToInt(stack -> stack.getAmount()).sum();
			int accepted = maximum - current;
			int amount = preItem.getAmount();
			
			boolean isChestItem = slot <= lastCommunitySlot;
			
			if(isChestItem){
				if(amount > accepted){
					rejectSlots.put(slot, newItems.get(slot).getAmount());
				}else{
					setChest.put(slot, finalItems.get(slot));
				}
			}else{
				setPlayer.put(slot, finalItems.get(slot));
			}
		}
		
		//set chest inventory slots
		for(Integer slot : setChest.keySet()){
			inventoryView.setItem(slot, setChest.get(slot));
		}
		
		//set player inventory slots
		for(Integer slot : setPlayer.keySet()){
			inventoryView.setItem(slot, setPlayer.get(slot));
		}
		
		//remove rejected slots?
		for(Integer slot : rejectSlots.keySet()){
			Bukkit.getScheduler().runTaskLater(this.plugin, new Runnable(){

				@Override
				public void run() {
					inventoryView.setItem(slot, oldItems.get(slot));
				}
				
			}, 1L);
		}
		
//		//setting the cursor last.
		int amount = leftOver + rejectSlots.values().stream().mapToInt(size -> size).sum();
		amount = (!setChest.isEmpty() ? amount : amount-(setChest.values().stream().mapToInt(stack -> stack.getAmount()).sum()));
		amount = (!setPlayer.isEmpty() ? amount : amount-(setPlayer.values().stream().mapToInt(stack -> stack.getAmount()).sum()));
		
		
		
		typeStack.setAmount(amount);
		event.setCursor(typeStack);
	}
	
	
	
	
	

	private HashMap<Integer, ItemStack> getNewItems(InventoryView inventory, Map<Integer, ItemStack> finalItems) {
		HashMap<Integer, ItemStack> itemsToAdd = new HashMap<>();
		for (Integer slot : finalItems.keySet()) {
			itemsToAdd.put(slot, getAddingStack(inventory, slot, finalItems.get(slot)));
		}
		return itemsToAdd;
	}
	
	private HashMap<Integer, ItemStack> getOldItems(InventoryView inventory, List<Object> slots) {
		HashMap<Integer, ItemStack> oldItems = new HashMap<>();
		for(Object slot : slots){
			int slotNum = (Integer)slot;
			oldItems.put(slotNum, inventory.getItem(slotNum));
		}
		return oldItems;
	}

	private ItemStack getAddingStack(InventoryView inventory, Integer slot, ItemStack finalStack) {
		ItemStack previousStack = getPreviousStack(inventory, slot);
		Integer previousAmount = 0;
		if (!previousStack.getType().equals(Material.AIR)) {
			previousAmount = previousStack.getAmount();
		}

		ItemStack preAdd = finalStack.clone();
		Integer finalAmount = preAdd.getAmount();

		Integer delta = finalAmount - previousAmount;
		preAdd.setAmount(delta);

		return preAdd;
	}

	private ItemStack getPreviousStack(InventoryView inventory, Integer slot) {
		ItemStack previousStack = inventory.getItem(slot);
		if (previousStack != null && previousStack.getType() != null) {
			return previousStack;
		} else {
			return new ItemStack(Material.AIR);
		}
	}

}