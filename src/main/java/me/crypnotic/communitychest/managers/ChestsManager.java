package me.crypnotic.communitychest.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;

import me.crypnotic.communitychest.CommunityChestPlugin;
import me.crypnotic.communitychest.api.AbstractConfig;
import me.crypnotic.communitychest.api.CommunityChest;

public class ChestsManager {

	private final CommunityChestPlugin plugin;
	private final AbstractConfig chestsConfig;
	private final HashMap<Location, CommunityChest> chests;

	public ChestsManager(CommunityChestPlugin plugin) {
		this.plugin = plugin;
		this.chestsConfig = plugin.getChestsConfig();
		this.chests = new HashMap<Location, CommunityChest>();
	}

	public void init() {
		init: {
			List<String> serials = chestsConfig.getStringList("chests");
			if (serials == null || serials.isEmpty()) {
				break init;
			}
			for (String serial : serials) {
				Block block = deserialize(serial);
				if (block == null || !(block.getState() instanceof Chest)) {
					continue;
				}
				CommunityChest chest = new CommunityChest(block.getLocation(), (Chest) block.getState());
				chests.put(block.getLocation(), chest);
			}
		}
		plugin.getLogger().info("Chests successfully loaded: " + chests.size());
	}

	public void save() {
		List<String> serials = new ArrayList<String>();
		if (!chests.isEmpty()) {
			for (CommunityChest chest : chests.values()) {
				serials.add(serialize(chest.getChest().getBlock()));
			}
		}
		chestsConfig.set("chests", chests.keySet());
	}
	
	public void clean(){
		save();
		chests.clear();
	}

	private Block deserialize(String serial) {
		String[] implode = serial.split(";");
		if (implode == null || implode.length != 4) {
			return null;
		}
		World world = plugin.getServer().getWorld(implode[0]);
		if (world == null) {
			return null;
		}
		return world.getBlockAt(Integer.valueOf(implode[1]), Integer.valueOf(implode[2]), Integer.valueOf(implode[3]));
	}

	private String serialize(Block block) {
		return String.format("%s;%d;%d;%d", block.getWorld().getName(), block.getX(), block.getY(), block.getZ());
	}

	public CommunityChest getCommunityChest(Inventory inventory) {
		for (CommunityChest chest : chests.values()) {
			if (chest.getInventory().equals(inventory)) {
				return chest;
			}
		}
		return null;
	}

	public boolean isCommunityChest(Block block) {
		return chests.containsKey(block.getLocation());
	}

	public boolean isCommunityChest(Inventory inventory) {
		for (CommunityChest chest : chests.values()) {
			if (chest.getInventory().equals(inventory)) {
				return true;
			}
		}
		return false;
	}
}
