package me.crypnotic.communitychest.api;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;

import lombok.Getter;

public class CommunityChest {

	private static final int INTERACT_COOLDOWN = 5;

	@Getter
	private final Location location;
	@Getter
	private final Chest chest;
	private final HashMap<UUID, Long> interactions;

	public CommunityChest(Location location, Chest chest) {
		this.location = location;
		this.chest = chest;
		this.interactions = new HashMap<UUID, Long>();
	}

	public Inventory getInventory() {
		return chest.getBlockInventory();
	}

	public Boolean interact(UUID uuid) {
		if (interactions.containsKey(uuid)) {
			Long current = System.currentTimeMillis();
			Long timestamp = interactions.get(uuid);
			if ((current - timestamp) >= (INTERACT_COOLDOWN * 60000)) {
				interactions.remove(uuid);
				interactions.put(uuid, current);
				return true;
			}
			return false;
		} else {
			interactions.put(uuid, System.currentTimeMillis());
			return true;
		}
	}
}
