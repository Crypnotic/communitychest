package me.crypnotic.communitychest.api;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class AbstractConfig {

	private final JavaPlugin plugin;
	private final File folder;
	private final File file;
	private YamlConfiguration configuration;

	public AbstractConfig(JavaPlugin plugin, File folder, String name) {
		this(plugin, folder, new File(folder, name));
	}

	public AbstractConfig(JavaPlugin plugin, File folder, File file) {
		this.plugin = plugin;
		this.folder = folder;
		this.file = file;
	}

	public boolean init(boolean resource) {
		if (!folder.exists()) {
			folder.mkdirs();
		}
		if (resource && !file.exists()) {
			try {
				plugin.saveResource(file.getName(), false);
			} catch (IllegalArgumentException exception) {
				return false;
			}
		} else if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException exception) {
				return false;
			}
		}
		this.configuration = YamlConfiguration.loadConfiguration(file);
		return true;
	}

	public void reload() {
		this.configuration = YamlConfiguration.loadConfiguration(file);
	}

	public boolean save() {
		try {
			configuration.save(file);
			return true;
		} catch (IOException exception) {
			return false;
		}
	}

	public void set(String path, Object value) {
		configuration.set(path, value);
	}

	public String getString(String path) {
		return configuration.getString(path);
	}

	public String getString(String path, String fallback) {
		String value = getString(path);
		if (value == null) {
			set(path, fallback);
			save();
		}
		return (value != null) ? value : fallback;
	}

	public Boolean getBoolean(String path) {
		return configuration.getBoolean(path);
	}

	public Boolean getBoolean(String path, boolean fallback) {
		Boolean value = getBoolean(path);
		if (value == null) {
			set(path, fallback);
			save();
		}
		return (value != null) ? value : fallback;
	}

	public List<String> getStringList(String path) {
		return configuration.getStringList(path);
	}

	public boolean contains(String path) {
		return configuration.contains(path);
	}
}
