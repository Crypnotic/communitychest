package me.crypnotic.communitychest;

import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import me.crypnotic.communitychest.api.AbstractConfig;
import me.crypnotic.communitychest.listeners.PlayerListener;
import me.crypnotic.communitychest.managers.ChestsManager;

public class CommunityChestPlugin extends JavaPlugin {

	@Getter
	private AbstractConfig chestsConfig;
	@Getter
	private ChestsManager chestsManager;

	@Override
	public void onLoad() {
		this.chestsConfig = new AbstractConfig(this, getDataFolder(), "chests.yml");

		this.chestsManager = new ChestsManager(this);
	}

	@Override
	public void onEnable() {
		if (!chestsConfig.init(true)) {
			getLogger().severe("Failed to load chests configuration file!");
		}

		chestsManager.init();

		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
	}

	@Override
	public void onDisable() {
		chestsManager.clean();
	}
}
